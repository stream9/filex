#ifndef FILEX_CORE_LOG_HPP
#define FILEX_CORE_LOG_HPP

#include "namespace.hpp"

#include <stream9/log.hpp>

namespace filex {

namespace log { using namespace st9::log; }

} // namespace filex

#endif // FILEX_CORE_LOG_HPP
