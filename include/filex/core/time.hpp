#ifndef FILEX_CORE_TIME_HPP
#define FILEX_CORE_TIME_HPP

#include <chrono>

namespace filex {

using std::chrono::milliseconds;

} // namespace filex

#endif // FILEX_CORE_TIME_HPP
