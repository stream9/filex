#ifndef FILEX_CORE_ARRAY_HPP
#define FILEX_CORE_ARRAY_HPP

#include "namespace.hpp"

#include <stream9/array.hpp>
#include <stream9/array_view.hpp>

namespace filex {

using st9::array;
using st9::array_view;

} // namespace filex

#endif // FILEX_CORE_ARRAY_HPP
