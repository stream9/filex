#ifndef FILEX_CORE_FILE_ENTRY_HPP
#define FILEX_CORE_FILE_ENTRY_HPP

#include "file_size.hpp"
#include "json.hpp"
#include "namespace.hpp"
#include "optional.hpp"
#include "string.hpp"

#include <cstdint>

namespace filex {

class directory_entry;

class file_entry
{
public:
    // essential
    file_entry() = default;

    file_entry(string_view name);
    file_entry(string_view name, file_size_t n);

    file_entry(directory_entry& parent, string_view name);

    file_entry(directory_entry& parent,
               string_view name, file_size_t n);

    file_entry(file_entry const&) = delete;
    file_entry& operator=(file_entry const&) = delete;
    file_entry(file_entry&&) = delete;
    file_entry& operator=(file_entry&&) = delete;

    virtual ~file_entry() = default;

    // accessor
    cstring_view name() const noexcept { return m_name; }

    virtual file_size_t total_size() const noexcept { return m_total_size; }

    virtual file_size_t current_size() const noexcept;

    opt<directory_entry const&> parent() const noexcept { return m_parent; }

    // query
    string path() const;

    bool is_created() const noexcept { return m_updated; }
    bool is_modified() const noexcept { return m_updated; }
    bool is_deleted() const noexcept { return m_deleted; }

    std::uint32_t mask() const noexcept { return m_mask; }

    // modifier
    template<typename T>
    void set_name(T&& n)
        requires std::is_constructible_v<string, T>
    {
        m_name = string(std::forward<T>(n));
    }

    void set_mask(std::uint32_t const mask) noexcept
    {
        m_mask = mask;
    }

    void set_created(bool v = true) noexcept
    {
        m_created = v;
    }

    virtual void set_modified(bool v = true) noexcept
    {
        m_updated = v;
    }

    void set_deleted(bool v = true) noexcept
    {
        m_deleted = v;
    }

    // comparison
    bool operator==(file_entry const& o) const noexcept
    {
        return m_name == o.m_name;
    }

    auto operator<=>(file_entry const& o) const noexcept
    {
        return m_name <=> o.m_name;
    }

private:
    string m_name;
    opt<directory_entry&> m_parent;
    file_size_t m_total_size = 0;
    mutable file_size_t m_current_size = 0;
    std::uint32_t m_mask;
    bool m_created : 1 = false;
    mutable bool m_updated : 1 = false;
    bool m_deleted : 1 = false;
};

void tag_invoke(json::value_from_tag, json::value&, file_entry const&);

} // namespace filex

#endif // FILEX_CORE_FILE_ENTRY_HPP
