#ifndef FILEX_CORE_COMMAND_HPP
#define FILEX_CORE_COMMAND_HPP

#include "array.hpp"
#include "error.hpp"
#include "file_entry.hpp"
#include "namespace.hpp"
#include "node.hpp"
#include "progress.hpp"
#include "string.hpp"
#include "time.hpp"

#include <cstdint>
#include <string_view>

#include <unistd.h>

#include <stream9/args.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/push_back.hpp>

namespace filex {

class command
{
public:
    // essential
    command(std::string_view path);

    virtual ~command() = default;

    // accessor
    auto& targets() { return m_targets; }
    auto& targets() const { return m_targets; }
    auto& argv() { return m_argv; }

    // modifier
    template<typename T, typename... Args>
    void add_args(T&& arg, Args&&... rest)
        requires std::is_convertible_v<T, std::string_view>
    {
        st9::push_back(m_argv, std::forward<T>(arg));
        add_args(std::forward<Args>(rest)...);
    }

    template<rng::input_range R, typename... Args>
    void add_args(R&& arg, Args&&... rest)
        requires std::is_convertible_v<rng::range_value_t<R>, std::string_view>
    {
        st9::append(m_argv, std::forward<R>(arg));
        add_args(std::forward<Args>(rest)...);
    }

    // command
    void start(progress_callback,
               error_callback,
               milliseconds interval);

    void print_targets() const;

protected:
    ::pid_t spawn_command(lx::fd_ref error);

    void monitor_progress(::pid_t,
                          lx::fd_ref error,
                          milliseconds interval,
                          progress_callback,
                          error_callback,
                          bool& changed);

    void report_progress(progress_callback);

    virtual void accumulate_progress(file_entry&, progress&) = 0;

    virtual std::uint32_t watch_mask() = 0;

private:
    void add_args() {}

private:
    string m_cmd;
    st9::args m_argv;
    array<node<file_entry>> m_targets;
};

} // namespace filex

#endif // FILEX_CORE_COMMAND_HPP
