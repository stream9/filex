#ifndef FILEX_CORE_NODE_HPP
#define FILEX_CORE_NODE_HPP

#include <stream9/node.hpp>

namespace filex {

using stream9::node;

} // namespace filex

#endif // FILEX_CORE_NODE_HPP
