#ifndef FILEX_CORE_DIRECTORY_ENTRY_HPP
#define FILEX_CORE_DIRECTORY_ENTRY_HPP

#include "array.hpp"
#include "file_entry.hpp"
#include "file_size.hpp"
#include "namespace.hpp"
#include "node.hpp"
#include "string.hpp"

namespace filex {

class file_entry;

class directory_entry : public file_entry
{
public:
    using entry_set = array<node<file_entry>>;
    using const_iterator = entry_set::const_iterator;

public:
    // essential
    directory_entry() = default;

    directory_entry(string_view path);

    directory_entry(directory_entry& parent, string_view path);

    // accessor
    const_iterator begin() const noexcept { return m_entries.begin(); }
    const_iterator end() const noexcept { return m_entries.end(); }

    // query
    const_iterator find(string_view const name) const noexcept;

    // modifier
    file_entry& add_file(string_view name);
    file_entry& add_file(string_view name, file_size_t n);

    directory_entry& add_directory(string_view path);

    void set_modified(bool v = true) noexcept override;

private:
    entry_set m_entries;
};

void tag_invoke(json::value_from_tag, json::value&, directory_entry const&);

} // namespace filex

#endif // FILEX_CORE_DIRECTORY_ENTRY_HPP
