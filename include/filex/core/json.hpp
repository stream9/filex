#ifndef FILEX_CORE_JSON_HPP
#define FILEX_CORE_JSON_HPP

#include "namespace.hpp"

#include <stream9/json.hpp>

namespace filex {

namespace json { using namespace st9::json; }

} // namespace filex

#endif // FILEX_CORE_JSON_HPP
