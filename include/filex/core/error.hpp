#ifndef FILEX_CORE_ERROR_HPP
#define FILEX_CORE_ERROR_HPP

#include "json.hpp"
#include "namespace.hpp"
#include "string.hpp"

#include <source_location>
#include <utility>

#include <stream9/errors.hpp>
#include <stream9/function_ref.hpp>

namespace filex {

using st9::error;

enum class errc {
    ok = 0,
    file_not_found,
    permission_denied,
    invalid_command_name,
    no_source_file,
    command_error,
    unknown_command_name,
};

std::error_category const& error_category() noexcept;

inline std::error_code
make_error_code(errc const e)
{
    return { static_cast<int>(e), error_category() };
}

using st9::print_error;
using st9::throw_error;
using st9::rethrow_error;

using error_callback = st9::function_ref<void(cstring_view)>;

} // namespace filex

namespace std {

template<>
struct is_error_code_enum<filex::errc> : true_type {};

} // namespace std

#endif // FILEX_CORE_ERROR_HPP
