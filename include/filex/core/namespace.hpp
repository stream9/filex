#ifndef FILEX_CORE_NAMESPACE_HPP
#define FILEX_CORE_NAMESPACE_HPP

namespace stream9::linux {}
namespace std::ranges {}

namespace filex {

namespace st9 { using namespace stream9; }
namespace lx { using namespace st9::linux; }
namespace rng { using namespace std::ranges; }

} // namespace filex

#endif // FILEX_CORE_NAMESPACE_HPP
