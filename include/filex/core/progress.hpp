#ifndef FILEX_CORE_PROGRESS_HPP
#define FILEX_CORE_PROGRESS_HPP

#include "file_size.hpp"

#include <stream9/function_ref.hpp>

namespace filex {

class file_entry;

struct progress
{
    file_entry const* last_updated_entry;
    file_size_t current_size;
    file_size_t total_size;
    file_size_t finished_count;
    file_size_t total_count;
};

using progress_callback = st9::function_ref<void(progress const&)>;

} // namespace filex

#endif // FILEX_CORE_PROGRESS_HPP
