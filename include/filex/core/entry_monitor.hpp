#ifndef FILEX_CORE_ENTRY_MONITOR_HPP
#define FILEX_CORE_ENTRY_MONITOR_HPP

#include "namespace.hpp"

#include <cstdint>
#include <thread>
#include <unordered_map>

#include <stream9/linux/epoll.hpp>
#include <stream9/linux/eventfd.hpp>
#include <stream9/linux/inotify.hpp>
#include <stream9/ref.hpp>

namespace filex {

class file_entry;

class entry_monitor
{
public:
    // essential
    entry_monitor(bool& changed);

    ~entry_monitor() noexcept;

    // modifier
    void add_entry(file_entry&, std::uint32_t mask);

    // command
    void start();

private:
    std::thread m_thread;
    lx::inotify m_inotify;
    lx::epoll<> m_epoll;
    lx::eventfd m_start_event;
    lx::eventfd m_stop_event;
    std::unordered_map<int, st9::ref<file_entry>> m_wd_entry;
    bool& m_changed;
};

} // namespace filex

#endif // FILEX_CORE_ENTRY_MONITOR_HPP
