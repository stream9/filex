#ifndef FILEX_CORE_STRING_HPP
#define FILEX_CORE_STRING_HPP

#include "namespace.hpp"

#include <string>
#include <string_view>

#include <stream9/cstring_view.hpp>
#include <stream9/cstring_ptr.hpp>

namespace filex {

using std::string;
using std::string_view;

using st9::cstring_view;
using st9::cstring_ptr;

} // namespace filex

#endif // FILEX_CORE_STRING_HPP
