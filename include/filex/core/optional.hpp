#ifndef FILEX_CORE_OPTIONAL_HPP
#define FILEX_CORE_OPTIONAL_HPP

#include <stream9/optional.hpp>

namespace filex {

using st9::optional;
using st9::opt;

} // namespace filex

#endif // FILEX_CORE_OPTIONAL_HPP
