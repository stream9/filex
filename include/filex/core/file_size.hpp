#ifndef FILEX_CORE_FILE_SIZE_HPP
#define FILEX_CORE_FILE_SIZE_HPP

#include <cstdint>

namespace filex {

using file_size_t = std::int64_t;

} // namespace filex

#endif // FILEX_CORE_FILE_SIZE_HPP
