#ifndef FILEX_CORE_COPY_TARGET_HPP
#define FILEX_CORE_COPY_TARGET_HPP

#include "array.hpp"
#include "directory_entry.hpp"
#include "node.hpp"
#include "string.hpp"

namespace filex {

array<node<file_entry>>
make_copy_target(array_view<string> src,
                 cstring_view dest,
                 bool dereference,
                 bool recursive);

} // namespace filex

#endif // FILEX_CORE_COPY_TARGET_HPP
