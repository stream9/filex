#include <stream9/qmanaged_ptr.hpp>

namespace filex::qt {

using stream9::qmanaged_ptr;
using stream9::make_qmanaged;

} // namespace filex:qt
