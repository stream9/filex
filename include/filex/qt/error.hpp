#ifndef FILEX_QT_ERROR_HPP
#define FILEX_QT_ERROR_HPP

#include <filex/core/error.hpp>
#include <filex/core/string.hpp>

#include <exception>

namespace filex::qt {

void displayError(std::exception_ptr) noexcept;
void displayError(string_view) noexcept;

} // namespace filex::qt

#endif // FILEX_QT_ERROR_HPP
