#ifndef STREAM9_FILEX_QT_META_TYPE_HPP
#define STREAM9_FILEX_QT_META_TYPE_HPP

#include <exception>

#include <QMetaType>

#include <filex/core/string.hpp>
#include <filex/core/progress.hpp>

Q_DECLARE_METATYPE(filex::string)
Q_DECLARE_METATYPE(filex::progress)
Q_DECLARE_METATYPE(std::exception_ptr)

#endif // STREAM9_FILEX_QT_META_TYPE_HPP
