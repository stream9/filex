#ifndef STREAM9_FILEX_QT_FILEX_HPP
#define STREAM9_FILEX_QT_FILEX_HPP

#include "error.hpp"
#include "meta_type.hpp"
#include "pointer.hpp"

#include <filex/core/progress.hpp>
#include <filex/core/string.hpp>

#include <QLabel>
#include <QDialog>
#include <QProgressBar>
#include <QSize>

#include <stream9/node.hpp>

namespace filex::qt {

class Command;

class FileX : public QDialog
{
    Q_OBJECT
public:
    FileX(int argc, char* argv[],
           QWidget* parent = nullptr,
           Qt::WindowFlags = {});

    ~FileX() noexcept;

    Q_SLOT void update(progress const&) noexcept;
    Q_SLOT void displayError(std::exception_ptr) noexcept;
    Q_SLOT void displayError2(string message) noexcept;
    Q_SLOT void close() noexcept;

private:
    st9::node<Command> m_command;
    qmanaged_ptr<QProgressBar> m_progBar;
    qmanaged_ptr<QLabel> m_sizeLabel;
    qmanaged_ptr<QLabel> m_countLabel;
    qmanaged_ptr<QLabel> m_nameLabel;
    bool m_displayingError : 1 = false;
    bool m_closeRequested : 1 = false;
};

} // namespace filex::qt

#endif // STREAM9_FILEX_QT_FILEX_HPP
