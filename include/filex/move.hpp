#ifndef FILEX_CORE_MOVE_CPP
#define FILEX_CORE_MOVE_CPP

#include "core/command.hpp"

namespace filex {

class move : public command
{
public:
    // essential
    template<typename... Args>
    move(Args&&... args)
        try : command { "/usr/bin/mv" }
    {
        this->add_args(std::forward<Args>(args)...);
        init();
    }
    catch (...) {
        rethrow_error();
    }

private:
    void init();

    void accumulate_progress(file_entry&, progress&) override;

    std::uint32_t watch_mask() override;
};

} // namespace filex

#endif // FILEX_CORE_MOVE_CPP
