#ifndef FILEX_CORE_COPY_HPP
#define FILEX_CORE_COPY_HPP

#include "core/command.hpp"
#include "core/error.hpp"
#include "core/string.hpp"

namespace filex {

class file_entry;
class progress;

class copy : public command
{
public:
    // essential
    template<typename... Args>
    copy(Args&&... args)
        try : command { "/usr/bin/cp" }
    {
        this->add_args(std::forward<Args>(args)...);
        init();
    }
    catch (...) {
        rethrow_error();
    }

private:
    void init();

    void accumulate_progress(file_entry&, progress&) override;

    std::uint32_t watch_mask() override;
};

} // namespace filex

#endif // FILEX_CORE_COPY_HPP
