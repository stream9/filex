#ifndef FILEX_CORE_REMOVE_HPP
#define FILEX_CORE_REMOVE_HPP

#include "core/command.hpp"
#include "core/error.hpp"
#include "core/file_entry.hpp"
#include "core/file_size.hpp"
#include "core/progress.hpp"
#include "core/string.hpp"
#include "core/time.hpp"

#include <chrono>
#include <functional>
#include <memory>
#include <vector>

namespace filex {

class remove : public command
{
public:
    // essential
    template<typename... Args>
    remove(Args&&... args)
        try : command { "/usr/bin/rm" }
    {
        this->add_args(std::forward<Args>(args)...);
        init();
    }
    catch (...) {
        rethrow_error();
    }

private:
    void init();

    void accumulate_progress(file_entry&, progress&) override;

    std::uint32_t watch_mask() override;
};

} // namespace filex

#endif // FILEX_CORE_REMOVE_HPP
