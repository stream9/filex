#include <filex/core/error.hpp>

namespace filex {

std::error_category const&
error_category() noexcept
{
    static struct impl : std::error_category
    {
        char const* name() const noexcept { return "filex"; }

        std::string
        message(int const ec) const
        {
            switch (static_cast<errc>(ec)) {
                using enum errc;
                case ok:
                    return "ok";
                case file_not_found:
                    return "File doesn't exist";
                case permission_denied:
                    return "Permission denied";
                case invalid_command_name:
                    return "Invalid command name";
                case no_source_file:
                    return "No source file is specified";
                case command_error:
                    return "Command error";
                case unknown_command_name:
                    return "Unknown command name";
                default:
                    return "Unknown error";
            }
        }
    } instance;

    return instance;
}

} // namespace filex
