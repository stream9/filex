#include <filex/core/entry_monitor.hpp>

#include <filex/core/directory_entry.hpp>
#include <filex/core/error.hpp>
#include <filex/core/file_entry.hpp>
#include <filex/core/log.hpp>
#include <filex/core/optional.hpp>

#include <functional>

#include <stream9/bits.hpp>
#include <stream9/log.hpp>

namespace filex {

static directory_entry*
to_directory(file_entry& e)
{
    return dynamic_cast<directory_entry*>(&e);
}

static file_entry*
find_child_entry(directory_entry& parent, cstring_view const name)
{
    auto const it = parent.find(name);
    if (it == parent.end()) {
        log::dbg() << "unknown target:" << name << parent.path();
        log::dbg() << json::value_from(parent);
        return {};
    }

    return &*it;
}

static directory_entry*
find_child_directory(directory_entry& parent, cstring_view const name)
{
    auto const it = parent.find(name);
    if (it == parent.end()) {
        log::dbg() << "unknown target:" << name << parent.path();
        log::dbg() << json::value_from(parent);
        return {};
    }

    return to_directory(*it);
}

static file_entry*
find_entry(auto& wd_entry, struct ::inotify_event const& ev)
{
    auto const it = wd_entry.find(ev.wd);
    if (it == wd_entry.end()) {
        log::dbg() << "unknownd wd:" << ev.wd;
        return {};
    }
    else {
        return &it->second;
    }
}

static void
handle_inotify_event(auto& inotify,
                     auto& wd_entry,
                     auto& changed)
{
    try {
        using st9::bits::any_of;
        using st9::bits::includes;

        char buf[1024];

        for (auto const& ev: inotify.read(buf)) {
            auto const o_ent = find_entry(wd_entry, ev);
            if (!o_ent) continue;

            if (includes(ev.mask, IN_CREATE)) {
                auto const o_dir = to_directory(*o_ent);
                if (o_dir) {
                    auto const o_cdir = find_child_directory(*o_dir, lx::name(ev));

                    if (o_cdir) {
                        auto const wd = inotify.add_watch(o_cdir->path(), o_dir->mask());
                        wd_entry.emplace(wd, *o_cdir);
                        o_cdir->set_created();
                        o_cdir->set_mask(o_dir->mask());
                        changed = true;
                    }
                }
            }
            else if (any_of(ev.mask, IN_MODIFY | IN_CLOSE_WRITE | IN_MOVED_TO)) {
                auto const o_dir = to_directory(*o_ent);
                if (o_dir) {
                    auto const o_cent = find_child_entry(*o_dir, lx::name(ev));
                    if (o_cent) {
                        o_cent->set_modified();
                        changed = true;
                    }
                }
            }
            else if (includes(ev.mask, IN_DELETE)) {
                auto const o_dir = to_directory(*o_ent);
                if (o_dir) {
                    auto const o_cent = find_child_entry(*o_dir, lx::name(ev));
                    if (o_cent) {
                        o_cent->set_deleted();
                        changed = true;
                    }
                }
            }
            if (includes(ev.mask, IN_DELETE_SELF)) {
                o_ent->set_deleted(true);
                changed = true;
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static void
run(lx::epoll<>& epoll,
    lx::inotify& inotify,
    auto& wd_entry,
    lx::eventfd const& stop_event,
    bool& changed)
{
    while (true) {
        for (auto const& ev: epoll.wait()) {
            if (ev.data.fd == inotify) {
                handle_inotify_event(inotify, wd_entry, changed);
            }
            else if (ev.data.fd == stop_event) {
                return;
            }
        }
    }
}

static void
add_directory(directory_entry& dir,
              auto& inotify,
              std::uint32_t const mask,
              auto& wd_entry)
{
    auto const wd = inotify.add_watch(dir.path(), mask);
    wd_entry.emplace(wd, dir);
    dir.set_mask(mask);

    for (auto& e: dir) {
        if (auto* const d = dynamic_cast<directory_entry*>(&e)) {
            try {
                add_directory(*d, inotify, mask, wd_entry);
            }
            catch (...) {
                // nop
            }
        }
    }
}

/*
 * class entry_monitor
 */
entry_monitor::
entry_monitor(bool& changed)
    try : m_changed { changed }
{
    m_epoll.add(m_inotify, EPOLLIN);
    m_epoll.add(m_stop_event, EPOLLIN);
}
catch (...) {
    rethrow_error();
}

entry_monitor::
~entry_monitor() noexcept
{
    try {
        m_stop_event.write(1);
        m_thread.join();
    }
    catch (...) {
        print_error(log::warn());
    }
}

void entry_monitor::
add_entry(file_entry& ent, std::uint32_t const mask)
{
    try {
        if (auto* const dir = to_directory(ent)) {
            add_directory(*dir, m_inotify, mask, m_wd_entry);
        }
        else {
            auto const wd = m_inotify.add_watch(ent.path(), mask);
            m_wd_entry.emplace(wd, ent);
            ent.set_mask(mask);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void entry_monitor::
start()
{
    try {
        m_thread = std::thread { [&] {
            try {
                m_start_event.write(1);
                run(m_epoll, m_inotify, m_wd_entry, m_stop_event, m_changed );
            }
            catch (...) {
                print_error(log::dbg());
            }
        } };

        m_start_event.read();
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace filex
