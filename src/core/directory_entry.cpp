#include <filex/core/directory_entry.hpp>

#include <filex/core/error.hpp>
#include <filex/core/optional.hpp>

#include <algorithm>

#include <stream9/ranges/binary_find.hpp>
#include <stream9/binary_insert.hpp>

namespace filex {

struct less
{
    bool operator()(file_entry const& lhs,
                    file_entry const& rhs) const noexcept
    {
        return lhs < rhs;
    }

    template<typename T>
    bool operator()(std::unique_ptr<T> const& lhs,
                    std::unique_ptr<T> const& rhs) const noexcept
    {
        return *lhs < *rhs;
    }

    template<typename T>
    bool operator()(file_entry const& lhs,
                    std::unique_ptr<T> const& rhs) const noexcept
    {
        return lhs < *rhs;
    }

    template<typename T>
    bool operator()(std::unique_ptr<T> const& lhs,
                    file_entry const& rhs) const noexcept
    {
        return *lhs < rhs;
    }
};

using st9::binary_insert;

/*
 * class directory_entry
 */
directory_entry::
directory_entry(string_view const path)
    try : file_entry { path }
{}
catch (...) {
    rethrow_error();
}

directory_entry::
directory_entry(directory_entry& parent, string_view const path)
    try : file_entry { parent, path }
{}
catch (...) {
    rethrow_error();
}

directory_entry::const_iterator directory_entry::
find(string_view const name) const noexcept
{
    using stream9::ranges::binary_find;

    auto proj = [](auto&& e) { return e->name(); };

    return binary_find(m_entries, name, std::less<void>(), proj);
}

file_entry& directory_entry::
add_file(string_view const name)
{
    try {
        auto const o_it =  binary_insert(m_entries,
            node<file_entry>(*this, name), less() );
        if (o_it) {
            return **o_it;
        }
        else {
            assert(false); //TODO throw error
        }
    }
    catch (...) {
        rethrow_error({ { "name", name }, });
    }
}

file_entry& directory_entry::
add_file(string_view name, file_size_t n)
{
    try {
        auto const o_it = binary_insert(m_entries,
            node<file_entry>(*this, name, n), less() );
        if (o_it) {
            return **o_it;
        }
        else {
            assert(false); //TODO throw error
        }
    }
    catch (...) {
        rethrow_error({
            { "name", name },
            { "n", n },
        });
    }
}

directory_entry& directory_entry::
add_directory(string_view path)
{
    try {
        auto const o_it = binary_insert(m_entries,
            node<directory_entry>(*this, path), less() );
        if (o_it) {
            auto it = *o_it;
            auto& n = *it;
            return static_cast<directory_entry&>(*n);
        }
        else {
            assert(false); //TODO throw error
        }
    }
    catch (...) {
        rethrow_error({
            { "path", path }
        });
    }
}

void directory_entry::
set_modified(bool const v) noexcept
{
    for (auto& ent: m_entries) {
        ent->set_modified(v);
    }
}

void
tag_invoke(json::value_from_tag, json::value& jv, directory_entry const& dir)
{
    auto& obj = jv.emplace_object();
    obj["name"] = dir.name();
    obj["total_size"] = dir.total_size();
    obj["current_size"] = dir.current_size();

    json::array arr;
    for (auto const& ent: dir) {
        arr.push_back(json::value_from(ent));
    }

    obj["entries"] = std::move(arr);
}

} // namespace filex
