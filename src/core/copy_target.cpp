#include <filex/core/copy_target.hpp>

#include <filex/core/error.hpp>
#include <filex/core/namespace.hpp>
#include <filex/core/file_entry.hpp>
#include <filex/core/log.hpp>

#include <stream9/emplace_back.hpp>
#include <stream9/linux/directory.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/linux/open.hpp>
#include <stream9/linux/stat.hpp>
#include <stream9/path/basename.hpp>
#include <stream9/path/split.hpp>

namespace filex {

using lx::fd_ref;

template<typename... Args>
node<directory_entry>&
emplace_back_directory(array<node<file_entry>>& entries,
                       Args&&... args)
{
    return reinterpret_cast<node<directory_entry>&>(
        st9::emplace_back(entries,
            node<directory_entry> { std::forward<Args>(args)... })
    );
}

static auto
stat(fd_ref const dirfd,
     cstring_view path,
     bool const dereference = true)
{
    auto const o_st = lx::nothrow::fstatat(
        dirfd,
        path,
        dereference ? 0 : AT_SYMLINK_NOFOLLOW
    );
    if (!o_st) {
        json::object cxt {
            { "path", path },
            { "dereference", dereference },
        };

        if (o_st == lx::errc::enoent) {
            throw error { errc::file_not_found, std::move(cxt), };
        }
        else if (o_st == lx::errc::eacces) {
            throw error { errc::permission_denied, std::move(cxt), };
        }
        else {
            throw error { "fstatat()", o_st.error(), std::move(cxt) };
        }
    }

    return *o_st;
}

static auto
stat(cstring_view path, bool const dereference = true)
{
    return stat(AT_FDCWD, path, dereference);
}

static bool
is_directory(cstring_view path, bool const dereference)
{
    return lx::is_directory(stat(path, dereference));
}

static bool
is_directory(struct ::dirent const& ent,
             fd_ref const dirfd,
             bool const dereference) noexcept
{
    try {
        if (ent.d_type == DT_DIR) {
            return true;
        }
        else if ((ent.d_type == DT_LNK && dereference) || ent.d_type == DT_UNKNOWN) {
            try {
                auto const st = stat(dirfd, lx::name(ent), dereference);
                return lx::is_directory(st);
            }
            catch (...) {
                return false;
            }
        }
        else {
            return false;
        }
    }
    catch (...) {
        return false;
    }
}

static file_size_t
file_size(fd_ref const parent,
          cstring_view const name,
          bool const dereference)
{
    try {
        auto const st = stat(parent, name, dereference);
        return st.st_size;
    }
    catch (...) {
        rethrow_error();
    }
}

static file_size_t
file_size(cstring_view const path,
          bool const dereference)
{
    try {
        auto const st = stat(path, dereference);
        return st.st_size;
    }
    catch (...) {
        rethrow_error();
    }
}

static void
add_source_directory(auto& tgt,
                     fd_ref const parent,
                     cstring_ptr const name,
                     bool const dereference)
{
    try {
        lx::directory src_dir { parent, name };
        for (auto const& ent: src_dir) {
            try {
                auto const n = lx::name(ent);
                if (n == "." || n == "..") continue;

                if (is_directory(ent, src_dir.fd(), dereference)) {
                    auto& dir = tgt.add_directory(n);

                    add_source_directory(dir, src_dir.fd(), n, dereference);
                }
                else {
                    auto const sz = file_size(src_dir.fd(), n, dereference);
                    tgt.add_file(n, sz);
                }
            }
            catch (...) {
                print_error(log::warn());
            }
        }
    }
    catch (...) {
        rethrow_error({
            { "name", name }
        });
    }
}

static void
add_source_entry(directory_entry& tgt,
                 cstring_view src_path,
                 bool const dereference)
{
    try {
        if (is_directory(src_path, dereference)) {
            auto [dname, fname] = st9::path::split(src_path);

            auto parent = lx::open(dname, O_RDONLY);

            auto& dir = tgt.add_directory(fname);

            add_source_directory(dir, parent, fname, dereference);
        }
        else {
            auto const sz = file_size(src_path, dereference);
            string fname { st9::path::basename(src_path) };
            tgt.add_file(fname, sz);
        }
    }
    catch (...) {
        rethrow_error({
            { "src_path", src_path }
        });
    }
}

enum source_mode {
    single_file = 0,
    single_directory,
    multiple_entries
};

static source_mode
get_source_mode(auto& src, bool const dereference)
{
    try {
        if (src.empty()) {
            throw error { errc::no_source_file, };
        }
        if (src.size() == 1) {
            if (is_directory(src[0], dereference)) {
                return single_directory;
            }
            else {
                return single_file;
            }
        }
        else {
            return multiple_entries;
        }
    }
    catch (...) {
        rethrow_error({
            { "dereference", dereference },
        });
    }
}

enum destination_mode {
    existent_directory = 0,
    existent_file,
    new_directory,
    new_file,
};

static destination_mode
get_destination_mode(auto& dest, source_mode src)
{
    auto o_st = lx::nothrow::stat(dest);
    if (o_st) {
        if (lx::is_directory(*o_st)) {
            return existent_directory;
        }
        else {
            return existent_file;
        }
    }
    else if (o_st == lx::errc::enoent) {
        if (src == single_file) {
            return new_file;
        }
        else {
            return new_directory;
        }
    }
    else if (o_st == lx::errc::eacces) {
        throw error {
            errc::permission_denied, { { "path", dest }, }
        };
    }
    else {
        throw error {
            "stat()", o_st.error(), { { "path", dest } }
        };
    }
}

array<node<file_entry>>
make_copy_target(array_view<string> const src,
                 cstring_view const dest,
                 bool const dereference,
                 bool const recursive)
{
    try {
        (void)recursive;

        array<node<file_entry>> targets;

        if (src.empty() || dest.empty()) return targets;

        auto& target = emplace_back_directory(targets);

        auto const src_mode = get_source_mode(src, dereference);
        auto const dest_mode = get_destination_mode(dest, src_mode);

        if (dest_mode == existent_directory) {
            target->set_name(dest);

            for (auto const& p: src) {
                try {
                    add_source_entry(target, p, dereference);
                }
                catch (...) {
                    print_error(log::warn());
                }
            }
        }
        else if (dest_mode == new_directory) {
            auto [dname, fname] = st9::path::split(dest);
            target->set_name(dname);

            if (src_mode == single_directory) {
                auto& dir = target->add_directory(fname);

                auto [src_dname, src_fname] = st9::path::split(src[0]);
                try {
                    auto src_parent = lx::open(src_dname, O_RDONLY);

                    add_source_directory(dir, src_parent, src_fname, dereference);
                }
                catch (...) {
                    print_error(log::dbg());
                }
            }
            else {
                auto& new_dir = target->add_directory(fname);
                for (auto const& p: src) {
                    try {
                        add_source_entry(new_dir, p, dereference);
                    }
                    catch (...) {
                        print_error(log::warn());
                    }
                }
            }
        }
        else {
            assert(dest_mode == existent_file || dest_mode == new_file);
            assert(src_mode != multiple_entries);

            auto [dname, fname] = st9::path::split(dest);
            target->set_name(dname);

            auto const sz = file_size(src[0], dereference);
            target->add_file(fname, sz);
        }

        return targets;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace filex
