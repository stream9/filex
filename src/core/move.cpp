#include <filex/move.hpp>

#include "copy_p.hpp"

#include <filex/core/array.hpp>
#include <filex/core/copy_target.hpp>
#include <filex/core/error.hpp>
#include <filex/core/log.hpp>
#include <filex/core/string.hpp>

#include <getopt.h>

#include <stream9/args.hpp>
#include <stream9/linux/inotify.hpp>
#include <stream9/push_back.hpp>

namespace filex {

static void
parse_option(st9::args& argv_,
             auto& srcs,
             auto& dest)
{
    try {
        int argc = argv_.size();
        auto argv = const_cast<char**>(argv_.data());
        static struct ::option long_options[] = {
            { nullptr, 0, 0, 0 },
        };

        ::optind = 0;
        ::opterr = 0; // no error message
        while (true) {
            int c = ::getopt_long(
                argc, argv,
                "", long_options, nullptr );
            if (c == -1) break;
        }

        if (argc - optind < 2) return;

        auto last = argc - 1;
        for (auto i = optind; i <= last - 1; ++i) {
            st9::push_back(srcs, argv[i]);
        }

        dest = argv[last];
    }
    catch (...) {
        rethrow_error();
    }
}

/*
 * class move
 */
void move::
init()
{
    try {
        array<string> paths;
        string dest;

        parse_option(this->argv(), paths, dest);

        this->targets() = make_copy_target(paths, dest, false, true);
    }
    catch (...) {
        rethrow_error();
    }
}

void move::
accumulate_progress(file_entry& e, progress& p)
{
    if (auto* d = dynamic_cast<directory_entry*>(&e)) {
        compute_progress(*d, p);
    }
    else {
        log::dbg() << "target should be a directory_entry:" << e.path();
    }
}

std::uint32_t move::
watch_mask()
{
    return IN_CLOSE_WRITE | IN_CREATE | IN_MODIFY | IN_MOVED_TO;
}

} // namespace filex
