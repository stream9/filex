#ifndef FILEX_CORE_COPY_P_HPP
#define FILEX_CORE_COPY_P_HPP

namespace filex {

class directory_entry;
class progress;

void
compute_progress(directory_entry&, progress&) noexcept;

} // namespace filex

#endif // FILEX_CORE_COPY_P_HPP
