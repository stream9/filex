#include <filex/copy.hpp>

#include <filex/core/array.hpp>
#include <filex/core/copy_target.hpp>
#include <filex/core/directory_entry.hpp>
#include <filex/core/file_entry.hpp>
#include <filex/core/log.hpp>

#include <getopt.h>

#include <stream9/args.hpp>
#include <stream9/push_back.hpp>
#include <stream9/linux/inotify.hpp>

namespace filex {

static void
parse_option(st9::args& argv_,
             auto& paths,
             auto& dest,
             bool& dereference,
             bool& recursive)
{
    try {
        int argc = argv_.size();
        auto argv = const_cast<char**>(argv_.data());
        static struct ::option long_options[] = {
            { "dereference", no_argument, 0, 'L' },
            { "no-dereference", no_argument, 0, 'P' },
            { "recursive", no_argument, 0, 'r' },
            { nullptr, 0, 0, 0 },
        };

        ::opterr = 0; // no error message
        while (true) {
            int c = ::getopt_long(argc, argv, "rRLP", long_options, nullptr );
            if (c == -1) break;

            switch (c) {
                case 'L':
                    dereference = true;
                    break;
                case 'P':
                    dereference = false;
                    break;
                case 'r':
                case 'R':
                    recursive = true;
                    break;
            }
        }

        if (argc - optind < 2) return;

        auto last = argc - 1;
        for (auto i = optind; i <= last - 1; ++i) {
            st9::push_back(paths, argv[i]);
        }

        dest = argv[last];
    }
    catch (...) {
        rethrow_error();
    }
}

static void
force_refresh(file_entry& e)
{
    e.set_modified(true);
}

void
compute_progress(directory_entry& dir, progress& p) noexcept
{
    auto const is_created = dir.is_created();
    dir.set_created(false);

    for (auto& e: dir) {
        if (is_created) force_refresh(e);

        try {
            if (auto* const d = dynamic_cast<directory_entry*>(&e)) {
                compute_progress(*d, p);
            }
            else {
                if (e->is_modified()) {
                    p.last_updated_entry = &e;
                }
                p.total_size += e->total_size();
                p.current_size += e->current_size();
                ++p.total_count;
                if (e->current_size() == e->total_size()) {
                    ++p.finished_count;
                }
            }
        }
        catch (...) {
            print_error(log::warn());
        }
    }
}

/*
 * class copy
 */
void copy::
init()
{
    try {
        array<string> paths;
        string dest;
        bool dereference = false;
        bool recursive = false;

        parse_option(this->argv(), paths, dest, dereference, recursive);

        this->targets() = make_copy_target(paths, dest, dereference, recursive);
    }
    catch (...) {
        rethrow_error();
    }
}

void copy::
accumulate_progress(file_entry& e, progress& p)
{
    if (auto* d = dynamic_cast<directory_entry*>(&e)) {
        compute_progress(*d, p);
    }
    else {
        log::dbg() << "target should be a directory_entry:" << e.path();
    }
}

std::uint32_t copy::
watch_mask()
{
    return IN_CLOSE_WRITE | IN_CREATE | IN_MODIFY | IN_MOVED_TO;
}

} // namespace filex
