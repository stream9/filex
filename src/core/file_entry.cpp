#include <filex/core/file_entry.hpp>

#include <filex/core/directory_entry.hpp>
#include <filex/core/error.hpp>
#include <filex/core/json.hpp>

#include <stream9/linux/stat.hpp>
#include <stream9/path/concat.hpp> // operator/

namespace filex {

file_entry::
file_entry(string_view const name)
    try : m_name { name }
{}
catch (...) {
    rethrow_error({
        { "name", name }
    });
}

file_entry::
file_entry(string_view const name, file_size_t const n)
    try : m_name { name }
        , m_total_size { n }
{}
catch (...) {
    rethrow_error({
        { "name", name },
        { "n", n },
    });
}

file_entry::
file_entry(directory_entry& parent, string_view const name)
    try : m_name { name }
        , m_parent { parent }
{}
catch (...) {
    rethrow_error({
        { "parent", parent.path() },
        { "name", name },
    });
}

file_entry::
file_entry(directory_entry& parent,
           string_view const name, file_size_t const n)
    try : m_name { name }
        , m_parent { parent }
        , m_total_size { n }
{}
catch (...) {
    rethrow_error({
        { "parent", parent.path() },
        { "name", name },
        { "n", n },
    });
}

file_size_t file_entry::
current_size() const noexcept
{
    if (m_updated) {
        auto const o_st = lx::nothrow::stat(path());
        if (o_st) {
            m_current_size = o_st->st_size;
        }
        else {
            m_current_size = 0;
        }
        m_updated = false;
    }
    return m_current_size;
}

string file_entry::
path() const
{
    using st9::path::operator/;

    if (m_parent) {
        return m_parent->path() / m_name;
    }
    else {
        return m_name;
    }
}

void
tag_invoke(json::value_from_tag, json::value& jv, file_entry const& f)
{
    if (auto* const dir = dynamic_cast<directory_entry const*>(&f)) {
        tag_invoke(json::value_from_tag(), jv, *dir);
    }
    else {
        auto& obj = jv.emplace_object();
        obj["name"] = f.name();
        obj["total_size"] = f.total_size();
        obj["current_size"] = f.current_size();
    }
}

} // namespace filex
