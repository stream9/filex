#include <filex/remove.hpp>

#include <filex/core/array.hpp>
#include <filex/core/directory_entry.hpp>
#include <filex/core/error.hpp>
#include <filex/core/file_entry.hpp>
#include <filex/core/log.hpp>
#include <filex/core/namespace.hpp>
#include <filex/core/node.hpp>

#include <cstdint>
#include <thread>
#include <unordered_map>
#include <vector>
#include <iostream>

#include <getopt.h>

#include <stream9/bits.hpp>
#include <stream9/emplace_back.hpp>
#include <stream9/linux/directory.hpp>
#include <stream9/linux/dup.hpp>
#include <stream9/linux/epoll.hpp>
#include <stream9/linux/eventfd.hpp>
#include <stream9/linux/exec.hpp>
#include <stream9/linux/fork.hpp>
#include <stream9/linux/inotify.hpp>
#include <stream9/linux/pidfd.hpp>
#include <stream9/linux/pipe.hpp>
#include <stream9/linux/stat.hpp>
#include <stream9/linux/timerfd.hpp>
#include <stream9/linux/wait.hpp>
#include <stream9/push_back.hpp>
#include <stream9/ref.hpp>

namespace filex {

using st9::ref;
using lx::fd_ref;

template<typename... Args>
node<directory_entry>&
emplace_back_directory(array<node<file_entry>>& entries,
                       Args&&... args)
{
    return reinterpret_cast<node<directory_entry>&>(
        st9::emplace_back(entries,
            node<directory_entry> { std::forward<Args>(args)... })
    );
}

static void
parse_option(st9::args& argv_, auto& paths, bool& recursive)
{
    try {
        int argc = argv_.size();
        auto argv = const_cast<char**>(argv_.data());
        static struct ::option long_options[] = {
            { "recursive", no_argument, 0, 'r' },
            { nullptr, 0, 0, 0 },
        };

        ::optind = 0;
        ::opterr = 0; // no error message
        while (true) {
            int const c =
                ::getopt_long(argc, argv, "rR", long_options, nullptr);
            if (c == -1) break;

            switch (c) {
                case 'r':
                case 'R':
                    recursive = true;
                    break;
            }
        }

        if (argc - optind < 1) return;

        for (auto i = optind; i <= argc - 1; ++i) {
            st9::push_back(paths, argv[i]);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static void
populate_directory(directory_entry& dir);

static void
add_entry(directory_entry& dir,
          cstring_view const& name,
          struct ::stat const& st)
{
    if (lx::is_directory(st)) {
        auto& d = dir.add_directory(name);
        populate_directory(d);
    }
    else {
        dir.add_file(name, st.st_size);
    }
}

static void
populate_directory(directory_entry& dir)
{
    lx::directory d { dir.path() };

    for (auto const& ent: d) {
        auto const& n = lx::name(ent);
        if (n == "." || n == "..") continue;

        auto o_st = lx::nothrow::fstatat(d.fd(), n, AT_SYMLINK_NOFOLLOW);
        if (o_st) {
            add_entry(dir, n, *o_st);
        }
    }
}

static array<node<file_entry>>
make_targets(auto& paths, bool const recursive)
{
    try {
        (void)recursive;
        array<node<file_entry>> targets;

        for (auto const& p: paths) {
            auto const o_st = lx::nothrow::lstat(p);
            if (o_st) {
                if (lx::is_directory(*o_st)) {
                    auto& dir = emplace_back_directory(targets, p);
                    populate_directory(dir);
                }
                else {
                    st9::emplace_back(targets, p, o_st->st_size);
                }
            }
            else {
                print_error(log::dbg(), error {
                    "lstat()",
                    o_st.error(), {
                        { "path", p }
                    }
                });
            }
        }

        return targets;
    }
    catch (...) {
        rethrow_error();
    }
}

/*
 * class remove
 */
void remove::
init()
{
    try {
        array<string> paths;
        bool recursive = false;

        parse_option(this->argv(), paths, recursive);

        this->targets() = make_targets(paths, recursive);
    }
    catch (...) {
        rethrow_error();
    }
}

void remove::
accumulate_progress(file_entry& e, progress& p)
{
    if (auto* d = dynamic_cast<directory_entry*>(&e)) {
        ++p.total_count;
        if (d->is_deleted()) {
            ++p.finished_count;
        }
        for (auto& e: *d) {
            accumulate_progress(e, p);
        }
    }
    else {
        ++p.total_count;
        p.total_size += e.total_size();
        if (e.is_deleted()) {
            p.current_size += e.total_size();
            ++p.finished_count;
        }
    }
}

std::uint32_t remove::
watch_mask()
{
    return IN_DELETE | IN_DELETE_SELF;
}

} // namespace filex
