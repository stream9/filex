#include <filex/core/command.hpp>

#include <filex/core/entry_monitor.hpp>
#include <filex/core/json.hpp>
#include <filex/core/log.hpp>
#include <filex/core/file_entry.hpp>

#include <vector>

#include <stream9/path.hpp>
#include <stream9/linux/dup.hpp>
#include <stream9/linux/epoll.hpp>
#include <stream9/linux/exec.hpp>
#include <stream9/linux/fork.hpp>
#include <stream9/linux/pidfd.hpp>
#include <stream9/linux/pipe.hpp>
#include <stream9/linux/timerfd.hpp>
#include <stream9/linux/utility/read_all.hpp>
#include <stream9/linux/wait.hpp>

namespace filex {

command::
command(std::string_view path)
    try : m_cmd { path }
{
    st9::push_back(m_argv, st9::path::basename(m_cmd));
}
catch (...) {
    rethrow_error();
}

void command::
start(progress_callback prog_cb,
      error_callback error_cb,
      milliseconds const interval)
{
    try {
        bool changed = false;
        entry_monitor emon { changed };

        for (auto& ent: m_targets) {
            emon.add_entry(ent, this->watch_mask());
        }

        emon.start();

        auto error_pipe = lx::pipe(O_CLOEXEC);
        auto pid = spawn_command(error_pipe.write);
        error_pipe.write.close();

        monitor_progress(
            pid,
            error_pipe.read,
            interval,
            std::move(prog_cb),
            std::move(error_cb),
            changed
        );
    }
    catch (...) {
        rethrow_error();
    }
}

::pid_t command::
spawn_command(lx::fd_ref error)
{
    try {
        auto pid = lx::fork();
        if (pid == 0) {
            lx::dup(error, STDERR_FILENO);

            lx::execp(m_cmd, m_argv);
        }
        else {
            return pid.release();
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void command::
monitor_progress(::pid_t const pid,
                 lx::fd_ref const error,
                 milliseconds interval,
                 progress_callback prog_cb,
                 error_callback error_cb,
                 bool& changed)
{
    try {
        lx::epoll epoll;

        lx::pidfd proc_exit { pid };
        epoll.add(proc_exit, EPOLLIN);

        epoll.add(error, EPOLLIN);

        lx::timerfd timer { CLOCK_MONOTONIC };
        lx::set_interval(timer, interval);
        epoll.add(timer, EPOLLIN);

        while (true) {
            for (auto const& ev: epoll.wait()) {
                if (ev.data.fd == proc_exit) {
                    report_progress(prog_cb);

                    lx::waitpid(pid);
                    return;
                }
                else if (ev.data.fd == timer) {
                    timer.read();
                    if (changed) {
                        report_progress(prog_cb);
                        changed = false;
                    }
                }
                else if (ev.data.fd == error) {
                    std::string msg;
                    read_all(error, msg);
                    if (!msg.empty()) {
                        error_cb(msg);
                    }
                }
                else {
                    log::dbg() << "unknown event:" << ev.data.fd;
                }
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void command::
report_progress(progress_callback prog_cb)
{
    progress p {};

    for (auto& e: m_targets) {
        accumulate_progress(e, p);
    }

    prog_cb(p);
}

void command::
print_targets() const
{
    for (auto const& e: m_targets) {
        std::cout << json::value_from(e) << std::endl;
    }
}

} // namespace filex
