#include <filex/copy.hpp>
#include <filex/core/error.hpp>
#include <filex/core/log.hpp>
#include <filex/core/progress.hpp>
#include <filex/core/string.hpp>
#include <filex/move.hpp>
#include <filex/remove.hpp>

#include <iostream>

#include <stream9/linux/path.hpp>

namespace filex {

static void
print_progress(progress const& p) //TODO make it fancier
{
    std::cout << p.current_size << " / " << p.total_size << std::endl;
}

static void
print_command_error(cstring_view const s)
{
    std::cerr << s << std::flush;
}

} // namespace filex

int
main(int argc, char* argv[])
{
    using namespace filex;
    using namespace std::literals;

    try {
        auto const cmd = lx::paths::basename(argv[0]);
        array_view<char const*> args {
            const_cast<char const**>(argv) + 1,
            static_cast<std::size_t>(argc) - 1
        };
        if (cmd == "cpx") {
            filex::copy app { args };

            app.start(print_progress, print_command_error, 200ms);
        }
        else if (cmd == "rmx") {
            filex::remove app { args };

            app.start(print_progress, print_command_error, 200ms);
        }
        else if (cmd == "mvx") {
            filex::move app { args };

            app.start(print_progress, print_command_error, 200ms);
        }
        else {
            log::err() << "unknown command:" << cmd;
            return 1;
        }

        return 0;
    }
    catch (...) {
        print_error();
        return 1;
    }
}
